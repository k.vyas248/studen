/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package studentlearningsoftware;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 *
 * @author Kruti
 */
public class MediaPlayer extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try {
       Group root= new Group();
       Stage stage = new Stage();
        Media media = new Media("G:\\project\\tree_1.avi");
        javafx.scene.media.MediaPlayer player = new javafx.scene.media.MediaPlayer(media);
        MediaView view = new MediaView(player);
        root.getChildren().add(view);
        Scene scene = new Scene(root,400,400);
        stage.setScene(scene);
        stage.show();
        
        player.play();}
        catch (Exception e){}
 
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
