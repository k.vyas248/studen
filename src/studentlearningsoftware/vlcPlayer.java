/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package studentlearningsoftware;

/**
 *
 * @author Kruti
 */
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.component.EmbeddedMediaListPlayerComponent;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

public class vlcPlayer {

        private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
          private final EmbeddedMediaListPlayerComponent mediaListPlayerComponent;

        
  public static void main(String[] args) {
    NativeLibrary.addSearchPath( RuntimeUtil.getLibVlcLibraryName(), "C:\\Program Files\\VideoLAN\\VLC" );
    Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
    new vlcPlayer().start(args[0]); //712testclipNTSC
  }
  
  private vlcPlayer() {
            JFrame frame = new JFrame("Student Learning Software");
            frame.setSize(700, 500);
            mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
            mediaListPlayerComponent = new EmbeddedMediaListPlayerComponent();
//          JPanel panel=new JPanel();
//          panel.setSize(500, 300);
//          panel.setBackground(Color.BLUE);
//          frame.add(panel, BorderLayout.CENTER);
//          panel.add(mediaPlayerComponent);
        //    frame.setContentPane(mediaPlayerComponent);
            frame.setContentPane(mediaListPlayerComponent);

            frame.setLocation(100, 100);
            
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);

        //    mediaPlayerComponent.getMediaPlayer().playMedia(args);
          }
  
  private void start(String mrl) {
            // One line of vlcj code to add the media to the play-list...
            mediaListPlayerComponent.getMediaList().addMedia(mrl);
            //mediaListPlayerComponent.getMediaList().addMedia("G:\\project\\New folder\\circular linked list.mov");

            // Another line of vlcj code to play the media...
            mediaListPlayerComponent.getMediaListPlayer().play();
          }
}

